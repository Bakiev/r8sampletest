package com.example.r8.sample

class Component private constructor(private val name: String) {


    fun name(): String = name

    fun inject(): Boolean = true

    companion object {
        fun create(): Component = component

        private const val keepClassDeclaration = "Created"
        private val component: Component = Component(keepClassDeclaration)
    }
}