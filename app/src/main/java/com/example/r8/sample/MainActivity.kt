package com.example.r8.sample

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val component = Component.create()
        setContentView(R.layout.activity_main)
        findViewById<TextView>(R.id.caption).text = component.name()
    }
}