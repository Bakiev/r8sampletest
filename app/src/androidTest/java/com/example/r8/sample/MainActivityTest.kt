package com.example.r8.sample

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Rule
import org.junit.Test

class MainActivityTest {
    @get:Rule
    internal val rule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testMainActivity() {
        onView(withText(R.string.app_name)).check(matches(isDisplayed()))
        onView(withText(Component.create().name())).check(matches(isDisplayed()))
    }
}