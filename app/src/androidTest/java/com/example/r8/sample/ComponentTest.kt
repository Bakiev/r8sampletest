package com.example.r8.sample

import org.junit.Test
import com.google.common.truth.Truth.assertThat

class ComponentTest {

    @Test
    fun testComponent() {
        assertThat(Component.create().name()).isEqualTo("Created")
    }
}
