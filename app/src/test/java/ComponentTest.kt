import com.example.r8.sample.Component
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test

class ComponentTest {

    @Test(expected = Test.None::class)
    fun shouldCreateComponent() {
        Component.create().name() shouldBeEqualTo "Created"
    }
}